"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_grpc apps *

:details: lara_grpc app configuration. This provides a genaric 
         django app configuration mechanism.
         For more details see:
         https://docs.djangoproject.com/en/2.0/ref/applications/

:authors: 

:date: (creation)          

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

from django.apps import AppConfig

class LaraGrpcConfig(AppConfig):
    name = 'lara_grpc'
    # verbose_name = "enter a verbose name for your app: lara_grpc here - this will be used in the admin interface"
    # lara_app_icon = 'lara_grpc_icon.svg'  # this will be used to display an icon, e.g. in the main LARA menu.

