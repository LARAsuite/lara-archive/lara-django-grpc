"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_grpc admin *

:details: lara_grpc admin module admin backend configuration.
         - 
:authors: 

:date: (creation)          

.. note:: - generate with django-extension: 
            python manage.py admin_generator lara_grpc >> lara_grpc/admin.py
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

from django.contrib import admin

